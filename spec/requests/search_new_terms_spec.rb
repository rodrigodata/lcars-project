require 'rails_helper'
RSpec.describe "search_term_that_already_exists", type: :request do
  it "Search a question" do
  visit '/'
  # check if page loaded correctly
  expect(page).to have_selector('body')
  fill_in :term , with: "Create An New Google Account"
  click_button "Search"
  # page should have term searched
  expect(page).to have_content("Create An New Google Account")
  end
end

RSpec.describe "search_term_incomplete", type: :request do
  it "Search a question" do
  visit '/'
  # check if page loaded correctly
  expect(page).to have_selector('body')
  fill_in :term , with: "How do I"
  click_button "Search"
  # page should have term searched
  expect(page).to have_no_content("How do I")
  end
end

RSpec.describe "create_new_task_to_search", type: :request do
  it "Search a question" do
  visit '/tasks/new'
  # check if page loaded correctly
  expect(page).to have_selector('body')
  fill_in "Name" , with: "How Do I Create An New Account On Heroku"
  click_button "Create"
  # page should have created term available to be searched
  visit '/'
  expect(page).to have_content("How Do I Create An New Account On Heroku")
  end
end