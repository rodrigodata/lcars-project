class Task < ApplicationRecord
    def self.search(term)
    	if term && "#{term}" != ' '
    		before_split = "#{term}"
    		stop_words = [
    			"who ", "where ", "how ", "many ", "is", "do ", "my ", "i ",
    			"are ", "you ", "this "]
    		stop_words.each do |p|
    			term.gsub!(/#{p}/, '')
    		end
    		#key words that my user base will search for (generic search)
    		#key_words = [
    		#	"account ", "login ", "signup ", "pricing", "price",
    		#	"value", "much", "cancel", "credit card", "email",
    		#	"delete", "create", "payment", "hours", "reset",
    		#	"password"]

    		# if even after split stop_words the size of term was greater than
    		# 2 and has a key_word from my array, then and only then i'll
    		# save in the DB
    		if(term.split.size >= 2 )
    			#if(key_words.any?{ |word| term.include?(word)})
    				$redis.incr before_split.split.map(&:capitalize).join(' ')
    			#end
    		end
   		    # whotewise will return nothing and will not save search
            where('name LIKE ?', "%#{term}%")
    	else
    		all
    	end
    end
end
